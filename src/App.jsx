import { Routes, Route, Navigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { Header } from './components/Header';
import { Courses } from './components/Courses';
import { Registration } from './components/Registration';
import { Login } from './components/Login';
import { CourseInfo } from './components/CourseInfo/CourseInfo';
import { CourseForm } from './components/CourseForm';
import { PrivateRouter } from './components/PrivateRouter/PrivateRouter';
import { getUser, getCourses, getAuthors } from './store/selectors';
import { localApiInstance } from './servisces';
import { fetchCourses } from './store/courses/thunk';
import { fetchAuthors } from './store/authors/thunk';
import { fetchUsers } from './store/user/thunk';
import { useEffect } from 'react';

const App = () => {
	const dispatch = useDispatch();
	const { isAuth } = useSelector(getUser);
	const { courses } = useSelector(getCourses);
	const { authors } = useSelector(getAuthors);

	const { token } = useSelector(getUser);

	useEffect(() => {
		if (token) {
			localApiInstance.defaults.headers.common = {
				Authorization: token.result,
			};
			!courses.length && dispatch(fetchCourses());
			!authors.length && dispatch(fetchAuthors());
			dispatch(fetchUsers());
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [dispatch, token]);

	return (
		<>
			<Header />
			<Routes>
				{!isAuth ? (
					<>
						<Route path='registration' element={<Registration />} />
						<Route path='login' element={<Login />} />
						<Route path='*' element={<Navigate replace to='/login' />} />
					</>
				) : (
					<>
						<Route
							path='registration'
							element={<Navigate replace to='/courses' />}
						/>
						<Route path='login' element={<Navigate replace to='/courses' />} />
						<Route path='/' element={<Navigate replace to='/courses' />} />
						<Route
							path='courses'
							element={<Courses courseAuthors={authors} courses={courses} />}
						/>
						<Route
							path='courses/:courseId'
							element={
								<CourseInfo courseCards={courses} courseAuthors={authors} />
							}
						/>
						<Route
							path='courses/add'
							element={
								<PrivateRouter>
									<CourseForm />
								</PrivateRouter>
							}
						/>
						<Route
							path='courses/update/:courseId'
							element={
								<PrivateRouter>
									<CourseForm />
								</PrivateRouter>
							}
						/>
					</>
				)}
			</Routes>
		</>
	);
};

export default App;
