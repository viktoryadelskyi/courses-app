import React from 'react';
import styles from './Button.module.css';

export const Button = ({ label, handleClick, buttonStyle, type }) => (
	<>
		<button
			className={buttonStyle ? styles.ButtonPurple : styles.ButtonSquare}
			onClick={handleClick}
			type={type}
		>
			{label}
		</button>
	</>
);
