import React from 'react';
import styles from './Input.module.css';

export const Input = ({
	label,
	type,
	onChange,
	placeholder,
	value,
	inputStyle,
}) => {
	return (
		<>
			{label ? (
				<div className={styles.InputBlock}>
					<label>{label}</label>
					<input
						type={type}
						onChange={onChange}
						className={inputStyle ? styles.InputOrange : styles.InputOrangeLong}
						placeholder={placeholder}
						value={value}
					/>
				</div>
			) : (
				<>
					<label>{label}</label>
					<input
						type={type}
						onChange={onChange}
						className={inputStyle ? styles.InputOrange : styles.InputOrangeLong}
						placeholder={placeholder}
						value={value}
					/>
				</>
			)}
		</>
	);
};
