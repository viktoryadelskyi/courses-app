export const dateGenerator = (today) => {
	const year = today.getFullYear();
	let month = today.getMonth() + 1;
	let day = today.getDate();

	return `${day}/${month}/${year}`;
};
