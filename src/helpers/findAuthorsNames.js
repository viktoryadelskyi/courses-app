export const findAuthorsNames = (authors, courseAuthors) =>
	authors
		.map((author) => {
			return courseAuthors.find((el) => el.id === author)?.name;
		})
		.join(', ');
