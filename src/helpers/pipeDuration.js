export const pipeDuration = (totalMinutes) => {
	const minutes = totalMinutes % 60;
	const hours = Math.floor(totalMinutes / 60);

	return `${hours > 9 ? hours : `0${hours}`}:${
		minutes > 9 ? minutes : `0${minutes}`
	} hours`;
};
