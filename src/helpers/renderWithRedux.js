import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import { createReduxStore } from '../store';

// export const renderWithReduxAndRouterForTests = (component, preloadedState) => {
// 	const store = createReduxStore(preloadedState);
// 	return render(
// 		<Provider store={store}>
// 			<MemoryRouter>{component}</MemoryRouter>
// 		</Provider>
// 	);
// };

export function renderWithProviders(
	ui,
	{
		preloadedState = {},
		store = createReduxStore(preloadedState),
		...renderOptions
	} = {}
) {
	function Wrapper({ children }) {
		return (
			<Provider store={store}>
				<MemoryRouter>{children}</MemoryRouter>
			</Provider>
		);
	}

	return { store, ...render(ui, { wrapper: Wrapper, ...renderOptions }) };
}
