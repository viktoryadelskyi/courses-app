import { useState } from 'react';

export const useGetToken = () => {
	const token = localStorage.getItem('token');

	const [userToken, setUserToken] = useState(JSON.parse(token));

	return [userToken, setUserToken];
};
