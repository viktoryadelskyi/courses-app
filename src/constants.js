export const BUTTON_TYPE_SUBMIT = 'submit';
export const HEADER_OUT_BTN = 'Logout';
export const BUTTON_STYLE_PURPLE = 'ButtonPurple';
export const BUTTON_SHOW_COURSE = 'Show course';
export const BUTTON_SEARCH = 'Search';
export const BUTTON_ADD_COURSE = 'Add new course';
export const BUTTON_CREATE_COURSE = 'Create course';
export const BUTTON_CREATE_AUTHOR = 'Create author';
export const BUTTON_ADD_AUTHOR = 'Add author';
export const BUTTON_DELETE_AUTHOR = 'Delete author';
export const BUTTON_GO_BACK = '< Back to courses';
export const BUTTON_EDIT_LABEL = 'edit';
export const BUTTON_DELETE_LABEL = 'del';
export const BUTTON_UPDATE_COURSE_LABEL = 'Update course';

export const INPUT_TYPE_TEXT = 'text';
export const INPUT_TYPE_NUMBER = 'number';
export const INPUT_COURSES_PLACEHOLDER = 'Enter course name...';
export const INPUT_STYLE_ORANGE = 'InputOrange';
export const INPUT_STYLE_ORANGE_LONG = 'InputOrangeLong';

export const CREATE_COURSE_TITLE = 'Title';
export const CREATE_COURSE_DESCRIPTION = 'Description';
export const CREATE_COURSE_ADD_AUTHOR_TITLE = 'Add author';
export const CREATE_COURSE_ADD_DURATION_TITLE = 'Duration';
export const CREATE_COURSE_ADD_DURATION_TIME = 'Duration: ';
export const CREATE_COURSE_ADD_DURATION_INPUT =
	'Add duration(only numbers more than 0)';
export const CREATE_COURSE_ALERT_AUTHOR =
	'author name length should be at least 2 characters';
export const CREATE_COURSE_GLOBAL_ALERT = 'Please, fill in all fields';
export const CREATE_COURSE_DESCRIPTION_ALERT =
	'Description text length should be at least 2 characters';
export const DEFAULT_INITIAL_DURATION_VALUE = '00:00 hours';

export const CREATE_COURSE_TITLE_INPUT = 'Enter title...';
export const CREATE_COURSE_DESCRIPTION_INPUT = 'Enter description';
export const CREATE_COURSE_DURATION_INPUT = 'Enter duration in minutes';
export const CREATE_COURSE_AUTHOR_INPUT = 'Enter author name...';
export const CREATE_COURSE_AUTHOR_LABLE = 'Author name';
export const COURSE_AUTHORS_TITLE = 'Authors';
export const COURSE_AUTHOR_TITLE = 'Course authors';
export const COURSE_AUTHORS_LIST_IS_EMPTY = 'Authors list is empty';

export const FORM_REGISTRATION_TITLE = 'Registration';
export const FORM_REGISTRATION_LABEL_NAME = 'Name';
export const FORM_REGISTRATION_PLACEHOLDER_NAME = 'Enter name';
export const FORM_REGISTRATION_LABEL_EMAIL = 'Email';
export const FORM_REGISTRATION_PLACEHOLDER_EMAIL = 'Enter email';
export const FORM_REGISTRATION_LABEL_PASSWORD = 'Password';
export const FORM_REGISTRATION_PLACEHOLDER_PASSWORD = 'Enter password';
export const BUTTON_REGISTRATION = 'Registration';
export const FORM_REGISTRATION_LOGIN_REDIRECTION_TEXT =
	'If you have an account you can ';
export const FORM_REGISTRATION_LOGIN_REDIRECTION_LINK = 'Login';

export const FORM_LOGIN_TITLE = 'Login';
export const FORM_LOGIN_LABEL_EMAIL = 'Email';
export const FORM_LOGIN__PLACEHOLDER_EMAIL = 'Enter email';
export const FORM_LOGIN_LABEL_PASSWORD = 'Password';
export const FORM_LOGIN_PLACEHOLDER_PASSWORD = 'Enter password';
export const FORM_LOGIN_REGISTRATION_REDIRECTION_TEXT =
	'If you not have an account you can ';
export const FORM_LOGIN_REGISTRATION_REDIRECTION_LINK = 'Registration';
export const FORM_LOGIN_BUTTON_LABEL = 'Login';

export const mockedCoursesList = [
	{
		id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c551ba',
		title: 'JavaScript',
		description: `Lorem Ipsum is simply dummy text of the printing and
typesetting industry. Lorem Ipsum
 has been the industry's standard dummy text ever since the
1500s, when an unknown
 printer took a galley of type and scrambled it to make a type
specimen book. It has survived
 not only five centuries, but also the leap into electronic typesetting, remaining essentially u
 nchanged.`,
		creationDate: '8/3/2021',
		duration: 160,
		authors: [
			'27cc3006-e93a-4748-8ca8-73d06aa93b6d',
			'f762978b-61eb-4096-812b-ebde22838167',
		],
	},
	{
		id: 'b5630fdd-7bf7-4d39-b75a-2b5906fd0916',
		title: 'Angular',
		description: `Lorem Ipsum is simply dummy text of the printing and
typesetting industry. Lorem Ipsum
 has been the industry's standard dummy text ever since the
1500s, when an unknown
 printer took a galley of type and scrambled it to make a type
specimen book.`,
		creationDate: '10/11/2020',
		duration: 210,
		authors: [
			'df32994e-b23d-497c-9e4d-84e4dc02882f',
			'095a1817-d45b-4ed7-9cf7-b2417bcbf748',
		],
	},
];

export const mockedAuthorsList = [
	{
		id: '27cc3006-e93a-4748-8ca8-73d06aa93b6d',
		name: 'Vasiliy Dobkin',
	},
	{
		id: 'f762978b-61eb-4096-812b-ebde22838167',
		name: 'Nicolas Kim',
	},
	{
		id: 'df32994e-b23d-497c-9e4d-84e4dc02882f',
		name: 'Anna Sidorenko',
	},
	{
		id: '095a1817-d45b-4ed7-9cf7-b2417bcbf748',
		name: 'Valentina Larina',
	},
];

export const regExpEmail = new RegExp(
	// eslint-disable-next-line no-useless-escape
	/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
);
