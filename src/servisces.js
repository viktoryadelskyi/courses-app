import axios from 'axios';

export const localApiInstance = axios.create({
	baseURL: 'http://localhost:3000',
});

export const postLogin = (userData) => {
	return localApiInstance.post('login', userData);
};
export const postRegistration = (newUser) => {
	return localApiInstance.post('register', newUser);
};

export const getCourses = () => {
	return localApiInstance.get('courses/all');
};
export const getAuthors = () => {
	return localApiInstance.get('authors/all');
};

export const getUser = () => {
	return localApiInstance.get('users/me');
};
export const addCourses = (course) => {
	return localApiInstance.post('courses/add', course);
};
export const addAuthors = (name) => {
	return localApiInstance.post('authors/add', name);
};
export const updateCourses = (id, course) => {
	return localApiInstance.put(`courses/${id}`, course);
};
export const deleteCourses = (id) => {
	return localApiInstance.delete(`courses/${id}`);
};
export const deleteUser = () => {
	return localApiInstance.delete('logout');
};
