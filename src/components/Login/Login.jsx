import { Link, useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { userSlice } from '../../store/user/userSlice';
import { Button, Input } from '../../common';
import {
	FORM_LOGIN_TITLE,
	FORM_LOGIN_LABEL_EMAIL,
	FORM_LOGIN__PLACEHOLDER_EMAIL,
	FORM_LOGIN_LABEL_PASSWORD,
	FORM_LOGIN_PLACEHOLDER_PASSWORD,
	FORM_LOGIN_REGISTRATION_REDIRECTION_TEXT,
	FORM_LOGIN_REGISTRATION_REDIRECTION_LINK,
	FORM_LOGIN_BUTTON_LABEL,
	BUTTON_TYPE_SUBMIT,
	BUTTON_STYLE_PURPLE,
} from '../../constants';
import styles from './Login.module.css';
import { postLogin } from '../../servisces';
import { getUser } from '../../store/selectors';
const { userAuth, userName, userEmail, userToken, userPassword } =
	userSlice.actions;

export const Login = () => {
	const dispatch = useDispatch();
	const navigate = useNavigate();

	const { email, password } = useSelector(getUser);

	const goCourses = () => navigate('/courses');

	const handleSubmit = (e) => {
		e.preventDefault();

		if (email && password) {
			postLogin({ email, password }).then((response) => {
				localStorage.setItem('token', JSON.stringify(response.data));
				dispatch(userToken(response.data));
				dispatch(userAuth(true));
				dispatch(userName(response.data.user.name));
			});
			goCourses();
		} else {
			alert('enter valid data');
		}
	};

	return (
		<form className={styles.FormContainer} onSubmit={handleSubmit}>
			<h1>{FORM_LOGIN_TITLE}</h1>
			<Input
				placeholder={FORM_LOGIN__PLACEHOLDER_EMAIL}
				label={FORM_LOGIN_LABEL_EMAIL}
				onChange={(e) => dispatch(userEmail(e.target.value))}
			/>
			<Input
				placeholder={FORM_LOGIN_LABEL_PASSWORD}
				label={FORM_LOGIN_PLACEHOLDER_PASSWORD}
				type={FORM_LOGIN_LABEL_PASSWORD}
				onChange={(e) => dispatch(userPassword(e.target.value))}
			/>
			<Button
				label={FORM_LOGIN_BUTTON_LABEL}
				buttonStyle={BUTTON_STYLE_PURPLE}
				type={BUTTON_TYPE_SUBMIT}
			/>
			<p>
				{FORM_LOGIN_REGISTRATION_REDIRECTION_TEXT}
				<Link to='/registration'>
					{FORM_LOGIN_REGISTRATION_REDIRECTION_LINK}
				</Link>
			</p>
		</form>
	);
};
