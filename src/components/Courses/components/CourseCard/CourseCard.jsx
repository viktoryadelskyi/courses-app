import React from 'react';
import styles from './CourseCard.module.css';
import { BUTTON_SHOW_COURSE } from '../../../../constants';
import { CourseButtons } from '../CourseButtons/CourseButtons';
import { pipeDuration } from '../../../../helpers';
import { findAuthorsNames } from '../../../../helpers/findAuthorsNames';

export const CourseCard = ({
	courseId,
	title,
	duration,
	creationDate,
	description,
	authors,
	courseAuthors,
}) => {
	return (
		<div className={styles.Container} data-testid='courses-card'>
			<div className={styles.Description}>
				<p data-testid='title'>{title}</p>
				<p data-testid='description'>{description}</p>
			</div>
			<div className={styles.Info}>
				<p className={styles.Autors}>
					<span className={styles.TitleText}>Autors: </span>
					<span data-testid='authors-list'>
						{authors ? findAuthorsNames(authors, courseAuthors) : ''}
					</span>
				</p>
				<p>
					<span className={styles.TitleText}>Duration: </span>
					{pipeDuration(duration)}
				</p>
				<p>
					<span className={styles.TitleText}>Created: </span> {creationDate}
				</p>
				<CourseButtons label={BUTTON_SHOW_COURSE} courseId={courseId} />
			</div>
		</div>
	);
};
