import { CourseCard } from '../CourseCard/CourseCard';
import '@testing-library/jest-dom/extend-expect';
import { renderWithProviders } from '../../../../helpers/renderWithRedux';
import { mockedCourses } from '../../../../store/courses/coursesMock';
import { mockedAuthors } from '../../../../store/authors/authorsMock';
import { findAuthorsNames } from '../../../../helpers/findAuthorsNames';

describe('test course card', () => {
	test('display title', () => {
		const title = mockedCourses[0].title;
		const { queryByTestId } = renderWithProviders(<CourseCard title={title} />);
		expect(queryByTestId('title')).toHaveTextContent(/Test title/);
	});

	test('display description', () => {
		const description = mockedCourses[0].description;
		const { queryByText } = renderWithProviders(
			<CourseCard description={description} />
		);
		expect(queryByText(/Test description/)).toBeInTheDocument();
	});

	test('display duration in the correct format', () => {
		const duration = mockedCourses[0].duration;
		const { queryByText } = renderWithProviders(
			<CourseCard duration={duration} />
		);
		expect(
			queryByText(/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9] \b(\w*hours\w*)\b$/i)
		).toBeInTheDocument();
	});

	test('created date in the correct format', () => {
		const creationDate = mockedCourses[0].creationDate;
		const { queryByText } = renderWithProviders(
			<CourseCard creationDate={creationDate} />
		);
		expect(
			queryByText(
				/^([1-9]|[12][0-9]|3[1])[- /.]([1-9]|1[12])[- /.](19|20)\d\d$/i
			)
		).toBeInTheDocument();
	});

	test('courseCard should display authors list', () => {
		const authors = mockedCourses[0].authors;
		const componentNames = findAuthorsNames(authors, mockedAuthors);

		const { queryAllByTestId } = renderWithProviders(
			<CourseCard courseAuthors={mockedAuthors} authors={authors} />
		);
		expect(queryAllByTestId('authors-list')[0]).toHaveTextContent(
			componentNames
		);
	});
});
