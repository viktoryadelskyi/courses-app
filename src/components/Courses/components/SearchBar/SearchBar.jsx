import {
	INPUT_TYPE_TEXT,
	BUTTON_SEARCH,
	INPUT_COURSES_PLACEHOLDER,
	INPUT_STYLE_ORANGE,
	BUTTON_STYLE_PURPLE,
} from '../../../../constants';
import { coursesSlice } from '../../../../store/courses/coursesSlice';
import { Button, Input } from '../../../../common';
import { useDispatch } from 'react-redux';
const { setSearchInputValue, setFoundedCourses } = coursesSlice.actions;

export const SearchBar = () => {
	const dispatch = useDispatch();

	return (
		<div>
			<Input
				type={INPUT_TYPE_TEXT}
				placeholder={INPUT_COURSES_PLACEHOLDER}
				inputStyle={INPUT_STYLE_ORANGE}
				onChange={(e) => dispatch(setSearchInputValue(e.target.value))}
			/>
			<Button
				label={BUTTON_SEARCH}
				buttonStyle={BUTTON_STYLE_PURPLE}
				handleClick={() => dispatch(setFoundedCourses())}
			/>
		</div>
	);
};
