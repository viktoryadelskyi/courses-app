import { Button } from '../../../../common';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { coursesSlice } from '../../../../store/courses/coursesSlice';
import { getUser } from '../../../../store/selectors';
import {
	BUTTON_STYLE_PURPLE,
	BUTTON_DELETE_LABEL,
	BUTTON_EDIT_LABEL,
} from '../../../../constants';
import styles from './CourseButtons.module.css';
import { deleteCourse } from '../../../../store/courses/thunk';

export const CourseButtons = ({ courseId, label }) => {
	const dispatch = useDispatch();

	const { deleteCourseCard } = coursesSlice.actions;
	const { role } = useSelector(getUser);

	const deleteCard = (id) => {
		dispatch(deleteCourseCard(id));
		dispatch(deleteCourse(id));
	};
	return (
		<div className={styles.CourseButtonsContainer}>
			<Link to={`/courses/${courseId}`}>
				<Button buttonStyle={BUTTON_STYLE_PURPLE} label={label} />
			</Link>
			{role === 'admin' && (
				<>
					<Link to={`/courses/update/${courseId}`}>
						<Button label={BUTTON_EDIT_LABEL} />
					</Link>
					<Button
						handleClick={() => deleteCard(courseId)}
						label={BUTTON_DELETE_LABEL}
					/>
				</>
			)}
		</div>
	);
};
