import { renderWithProviders } from '../../../helpers/renderWithRedux';
import { mockedAuthors } from '../../../store/authors/authorsMock';
import { mockedCourses } from '../../../store/courses/coursesMock';
import '@testing-library/jest-dom/extend-expect';
import { Courses } from '../Courses';
import { fireEvent, waitFor } from '@testing-library/react';

describe('test courses component', () => {
	test('should display amount of CourseCard equal length of courses array', () => {
		const { queryAllByTestId } = renderWithProviders(
			<Courses courses={mockedCourses} courseAuthors={mockedAuthors} />
		);
		expect(queryAllByTestId('courses-card')).toHaveLength(mockedCourses.length);
	});
	test('should display Empty container if courses array length is 0', () => {
		const { queryByTestId } = renderWithProviders(<Courses />);
		expect(queryByTestId('courses-card')).not.toBeInTheDocument();
	});
	test('CourseForm should be showed after a click on a button "Add new course".', () => {
		const { queryByText, queryByTestId } = renderWithProviders(
			<Courses courses={mockedCourses} courseAuthors={mockedAuthors} />
		);
		fireEvent.click(queryByText(/Add new course/));
		waitFor(() => expect(queryByTestId('course-form')).toBeInTheDocument());
	});
});
