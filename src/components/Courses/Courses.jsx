import { v4 as uuidv4 } from 'uuid';
import { useNavigate } from 'react-router-dom';
import styles from './Courses.module.css';
import { CourseCard } from './components/CourseCard';
import { Button } from '../../common';
import { BUTTON_STYLE_PURPLE, BUTTON_ADD_COURSE } from '../../constants';
import { SearchBar } from './components/SearchBar';
import { useSelector, useDispatch } from 'react-redux';
import { getCourses } from '../../store/selectors';
import { useEffect } from 'react';
import { resetAuthorsList } from '../../store/authors/authorsSlice';

export const Courses = ({ courses, courseAuthors }) => {
	const navigate = useNavigate();
	const dispatch = useDispatch();

	const goToCreateCourses = () => navigate('/courses/add');
	const { foundedCourses } = useSelector(getCourses);

	useEffect(() => {
		dispatch(resetAuthorsList());
	}, [courses, dispatch]);

	return (
		<div className={styles.Container}>
			<div className={styles.CoursesControll}>
				<SearchBar />
				<Button
					label={BUTTON_ADD_COURSE}
					buttonStyle={BUTTON_STYLE_PURPLE}
					handleClick={goToCreateCourses}
				/>
			</div>
			{foundedCourses.length
				? foundedCourses.map((item) => {
						return (
							<CourseCard
								key={item.id}
								courseId={item.id}
								title={item.title}
								duration={item.duration}
								creationDate={item.creationDate}
								description={item.description}
								authors={item.authors}
								courseAuthors={courseAuthors}
							/>
						);
				  })
				: courses &&
				  courses.map((item) => {
						return (
							<CourseCard
								key={uuidv4()}
								courseId={item.id}
								title={item.title}
								duration={item.duration}
								creationDate={item.creationDate}
								description={item.description}
								authors={item.authors}
								courseAuthors={courseAuthors}
							/>
						);
				  })}
		</div>
	);
};
