import { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { Button, Input } from '../../common';
import {
	FORM_REGISTRATION_TITLE,
	FORM_REGISTRATION_LABEL_NAME,
	FORM_REGISTRATION_PLACEHOLDER_NAME,
	FORM_REGISTRATION_LABEL_EMAIL,
	FORM_REGISTRATION_PLACEHOLDER_EMAIL,
	FORM_REGISTRATION_LABEL_PASSWORD,
	FORM_REGISTRATION_PLACEHOLDER_PASSWORD,
	BUTTON_REGISTRATION,
	BUTTON_STYLE_PURPLE,
	FORM_REGISTRATION_LOGIN_REDIRECTION_TEXT,
	FORM_REGISTRATION_LOGIN_REDIRECTION_LINK,
	BUTTON_TYPE_SUBMIT,
	regExpEmail,
} from '../../constants';
import { postRegistration } from '../../servisces';
import styles from './Registration.module.css';

export const Registration = () => {
	const [userName, setUserName] = useState('');
	const [userEmail, setUserEmail] = useState('');
	const [userPassword, setUserPassword] = useState('');

	const navigate = useNavigate();
	const goLogin = () => navigate('/login');

	const handleSubmit = (e) => {
		e.preventDefault();
		if (userName.length < 4 || Number(userName)) {
			return alert('enter valid name');
		}
		if (!regExpEmail.test(userEmail)) {
			return alert('enter valid email');
		}
		if (userPassword.length <= 6) {
			return alert('password length must be more than 6');
		}
		if (
			userName.length > 2 &&
			regExpEmail.test(userEmail) &&
			userPassword.length >= 6
		) {
			const newUser = {
				name: userName,
				password: userPassword,
				email: userEmail,
			};
			localStorage.setItem('newUser', JSON.stringify(newUser));
			postRegistration(newUser);
			goLogin();
		}
	};

	return (
		<form className={styles.FormContainer} onSubmit={(e) => handleSubmit(e)}>
			<h1>{FORM_REGISTRATION_TITLE}</h1>
			<Input
				placeholder={FORM_REGISTRATION_PLACEHOLDER_NAME}
				label={FORM_REGISTRATION_LABEL_NAME}
				onChange={(e) => setUserName(e.target.value)}
			/>

			<Input
				placeholder={FORM_REGISTRATION_PLACEHOLDER_EMAIL}
				label={FORM_REGISTRATION_LABEL_EMAIL}
				onChange={(e) => setUserEmail(e.target.value)}
			/>
			<Input
				type={FORM_REGISTRATION_LABEL_PASSWORD}
				placeholder={FORM_REGISTRATION_PLACEHOLDER_PASSWORD}
				label={FORM_REGISTRATION_LABEL_PASSWORD}
				onChange={(e) => setUserPassword(e.target.value)}
			/>
			<Button
				label={BUTTON_REGISTRATION}
				buttonStyle={BUTTON_STYLE_PURPLE}
				type={BUTTON_TYPE_SUBMIT}
			/>
			<p>
				{FORM_REGISTRATION_LOGIN_REDIRECTION_TEXT}
				<Link to='/login'>{FORM_REGISTRATION_LOGIN_REDIRECTION_LINK}</Link>
			</p>
		</form>
	);
};
