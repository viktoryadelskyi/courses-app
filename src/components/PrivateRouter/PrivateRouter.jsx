import { useSelector } from 'react-redux';
import { useLocation, Navigate } from 'react-router-dom';
import { getUser } from '../../store/selectors';

export const PrivateRouter = ({ children }) => {
	const location = useLocation();
	const { role } = useSelector(getUser);

	if (role !== 'admin') {
		return <Navigate to='/courses' state={{ from: location }} />;
	}
	return children;
};
