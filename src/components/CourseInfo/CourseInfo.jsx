import { useParams, useNavigate } from 'react-router-dom';
import { Button } from '../../common';
import { BUTTON_STYLE_PURPLE, BUTTON_GO_BACK } from '../../constants';
import { pipeDuration } from '../../helpers';
import { findAuthorsNames } from '../../helpers/findAuthorsNames';
import styles from './CourseInfo.module.css';

export const CourseInfo = ({ courseCards, courseAuthors }) => {
	const { courseId } = useParams();

	const navigate = useNavigate();
	const goBack = () => navigate(-1);

	const course = courseCards.find((course) => course.id === courseId);

	return (
		<div className={styles.InfoContainer}>
			<Button
				handleClick={goBack}
				buttonStyle={BUTTON_STYLE_PURPLE}
				label={BUTTON_GO_BACK}
			/>
			{course && (
				<>
					<h1 className={styles.CourseTitle}>{course.title}</h1>
					<div className={styles.CourseCard}>
						<p className={styles.CourseDescription}>{course.description}</p>
						<div className={styles.CourseInfo}>
							<p>
								<span className={styles.BoldText}>ID:</span> {course.id}
							</p>
							<p>
								<span className={styles.BoldText}>Duration:</span>{' '}
								{pipeDuration(course.duration)}
							</p>
							<p>
								<span className={styles.BoldText}>Created:</span>{' '}
								{course.creationDate}
							</p>
							<p>
								<span className={styles.BoldText}>Authors:</span>{' '}
								{findAuthorsNames(course.authors, courseAuthors)}
							</p>
						</div>
					</div>
				</>
			)}
		</div>
	);
};
