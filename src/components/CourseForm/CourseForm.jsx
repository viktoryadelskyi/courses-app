import { useSelector, useDispatch } from 'react-redux';
import styles from './CreateCourse.module.css';
import {
	coursesSlice,
	resetCoursesInputs,
} from '../../store/courses/coursesSlice';
import {
	authorsSlice,
	resetAuthorsList,
} from '../../store/authors/authorsSlice';
import {
	BUTTON_CREATE_COURSE,
	BUTTON_STYLE_PURPLE,
	INPUT_TYPE_TEXT,
	INPUT_STYLE_ORANGE,
	INPUT_STYLE_ORANGE_LONG,
	CREATE_COURSE_TITLE_INPUT,
	CREATE_COURSE_DESCRIPTION_INPUT,
	CREATE_COURSE_TITLE,
	CREATE_COURSE_DESCRIPTION,
	CREATE_COURSE_ADD_AUTHOR_TITLE,
	CREATE_COURSE_AUTHOR_INPUT,
	BUTTON_CREATE_AUTHOR,
	CREATE_COURSE_AUTHOR_LABLE,
	CREATE_COURSE_ADD_DURATION_TITLE,
	CREATE_COURSE_ADD_DURATION_TIME,
	CREATE_COURSE_ADD_DURATION_INPUT,
	CREATE_COURSE_ALERT_AUTHOR,
	CREATE_COURSE_GLOBAL_ALERT,
	CREATE_COURSE_DESCRIPTION_ALERT,
	BUTTON_UPDATE_COURSE_LABEL,
	DEFAULT_INITIAL_DURATION_VALUE,
} from '../../constants';
import { dateGenerator } from '../../helpers';
import { Button, Input } from '../../common';
import { AuthorsCard } from './AuthorsCard/AuthorsCard';
import { v4 as uuidv4 } from 'uuid';
import { useNavigate, useParams } from 'react-router-dom';
import { getAuthors, getCourses } from '../../store/selectors';
import { useEffect } from 'react';
import {
	addCourse,
	fetchCourses,
	updateCourse,
} from '../../store/courses/thunk';
import { authorsAdd, fetchAuthors } from '../../store/authors/thunk';
const {
	setCourseTitle,
	setCourseDescription,
	setCourseDurationTime,
	setActiveCourse,
} = coursesSlice.actions;
const {
	setNewAuthorName,
	setSelectedAuthors,
	activeCourseSelectedAuthors,
	deleteAuthors,
} = authorsSlice.actions;

export const CourseForm = () => {
	const navigate = useNavigate();
	const dispatch = useDispatch();
	const goToCourses = () => navigate('/courses');

	const { courseId } = useParams();

	const { authors, newAuthorName, selectedAuthors } = useSelector(getAuthors);
	const { activeCourse } = useSelector(getCourses);

	const {
		minutesNumber,
		newCourseDurationTime,
		newCourseTitle,
		newCourseDescription,
	} = useSelector(getCourses);

	useEffect(() => {
		(async function () {
			await dispatch(fetchAuthors());
			dispatch(setActiveCourse(courseId));

			if (activeCourse && courseId) {
				dispatch(setCourseTitle(activeCourse.title));
				dispatch(setCourseDescription(activeCourse.description));
				dispatch(setCourseDurationTime(activeCourse.duration));
				dispatch(activeCourseSelectedAuthors(activeCourse.authors));
			}
			if (!activeCourse && !courseId) {
				dispatch(setCourseTitle(''));
				dispatch(setCourseDescription(''));
				dispatch(setCourseDurationTime(DEFAULT_INITIAL_DURATION_VALUE));
				dispatch(setActiveCourse(null));
			}
		})();
		return () => {
			dispatch(resetAuthorsList());
		};
	}, [dispatch, courseId, activeCourse]);

	const addAuthor = (author) => {
		dispatch(setSelectedAuthors(author));
	};

	const deleteAuthor = (author) => {
		dispatch(deleteAuthors(author));
	};

	const addNewAuthorOnclick = async () => {
		if (newAuthorName.length < 2) {
			return alert(CREATE_COURSE_ALERT_AUTHOR);
		}
		if (newAuthorName.length >= 2) {
			const author = { name: newAuthorName, id: uuidv4() };
			await dispatch(authorsAdd(author));
			await dispatch(fetchAuthors());
			activeCourse &&
				dispatch(activeCourseSelectedAuthors(activeCourse.authors));
		}
		dispatch(setNewAuthorName(''));
	};

	const addCourseCard = async () => {
		if (
			!newCourseTitle.length ||
			!newCourseDescription.length ||
			!selectedAuthors.length ||
			minutesNumber === DEFAULT_INITIAL_DURATION_VALUE
		) {
			return alert(CREATE_COURSE_GLOBAL_ALERT);
		}
		if (newCourseDescription.length < 2) {
			return alert(CREATE_COURSE_DESCRIPTION_ALERT);
		}
		if (activeCourse) {
			const activeCourseId = activeCourse.id;
			const courseData = {
				authors: selectedAuthors.map((author) => author.id),
				creationDate: activeCourse.creationDate,
				description: newCourseDescription,
				duration: Number(newCourseDurationTime),
				id: activeCourseId,
				title: newCourseTitle,
			};
			await dispatch(updateCourse({ activeCourseId, courseData }));
		}
		if (!activeCourse) {
			await dispatch(
				addCourse({
					title: newCourseTitle,
					description: newCourseDescription,
					creationDate: dateGenerator(new Date()),
					duration: Number(newCourseDurationTime),
					authors: selectedAuthors.map((author) => author.id),
					// id: uuidv4(),
				})
			);
		}
		dispatch(resetAuthorsList());
		dispatch(resetCoursesInputs());
		dispatch(fetchCourses());
		goToCourses();
	};

	return (
		<>
			<div className={styles.CreateCourseContainer} data-testid='courses-form'>
				<div className={styles.CreateTitle}>
					<Input
						type={INPUT_TYPE_TEXT}
						className={INPUT_STYLE_ORANGE}
						placeholder={CREATE_COURSE_TITLE_INPUT}
						onChange={(e) => dispatch(setCourseTitle(e.target.value))}
						label={CREATE_COURSE_TITLE}
						value={newCourseTitle}
					/>
					<Button
						label={
							activeCourse ? BUTTON_UPDATE_COURSE_LABEL : BUTTON_CREATE_COURSE
						}
						buttonStyle={BUTTON_STYLE_PURPLE}
						handleClick={addCourseCard}
					/>
				</div>

				<div>
					<p>{CREATE_COURSE_DESCRIPTION}</p>
					<textarea
						className={styles.CreateDescription}
						placeholder={CREATE_COURSE_DESCRIPTION_INPUT}
						onChange={(e) => dispatch(setCourseDescription(e.target.value))}
						value={newCourseDescription}
					/>
				</div>

				<div className={styles.AutorsContainer}>
					<div className={styles.LeftBlock}>
						<h1>{CREATE_COURSE_ADD_AUTHOR_TITLE}</h1>
						<div className={styles.AboutText}>
							<Input
								type={INPUT_TYPE_TEXT}
								className={INPUT_STYLE_ORANGE_LONG}
								placeholder={CREATE_COURSE_AUTHOR_INPUT}
								onChange={(e) => dispatch(setNewAuthorName(e.target.value))}
								value={newAuthorName}
								label={CREATE_COURSE_AUTHOR_LABLE}
							/>
						</div>

						<Button
							label={BUTTON_CREATE_AUTHOR}
							buttonStyle={BUTTON_STYLE_PURPLE}
							handleClick={() => addNewAuthorOnclick()}
						/>

						<h1>{CREATE_COURSE_ADD_DURATION_TITLE}</h1>
						<div className={styles.AboutText}>
							<Input
								className={INPUT_STYLE_ORANGE_LONG}
								placeholder={CREATE_COURSE_ADD_DURATION_INPUT}
								onChange={(e) =>
									dispatch(setCourseDurationTime(e.target.value))
								}
								value={newCourseDurationTime}
								label={CREATE_COURSE_ADD_DURATION_TITLE}
							/>
						</div>
						<p className={styles.LeftText}>
							{CREATE_COURSE_ADD_DURATION_TIME}
							{minutesNumber}
						</p>
					</div>
					<AuthorsCard
						courseAuthors={authors}
						selectedAuthors={selectedAuthors}
						addAuthor={addAuthor}
						deleteAuthor={deleteAuthor}
					/>
				</div>
			</div>
		</>
	);
};
