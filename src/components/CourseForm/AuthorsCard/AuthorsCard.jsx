import styles from './AuthorsCard.module.css';
import { Button } from '../../../common';

import {
	BUTTON_STYLE_PURPLE,
	BUTTON_ADD_AUTHOR,
	COURSE_AUTHOR_TITLE,
	COURSE_AUTHORS_TITLE,
	BUTTON_DELETE_AUTHOR,
	COURSE_AUTHORS_LIST_IS_EMPTY,
} from '../../../constants';

export const AuthorsCard = ({
	courseAuthors,
	selectedAuthors,
	addAuthor,
	deleteAuthor,
}) => {
	return (
		<div className={styles.AuthorsContainer}>
			<h1>{COURSE_AUTHORS_TITLE}</h1>
			{courseAuthors.length === 0 ? (
				<p>{COURSE_AUTHORS_LIST_IS_EMPTY}</p>
			) : (
				courseAuthors.map((author) => {
					return (
						<div className={styles.AuthorsBlock} key={author.id}>
							<p className={styles.AuthorsBlockText}>{author.name}</p>
							<div>
								<Button
									label={BUTTON_ADD_AUTHOR}
									buttonStyle={BUTTON_STYLE_PURPLE}
									handleClick={() => addAuthor(author)}
								/>
							</div>
						</div>
					);
				})
			)}

			<div className={styles.AddAuthorsBlock}>
				<h1>{COURSE_AUTHOR_TITLE}</h1>
				{selectedAuthors.length === 0 ? (
					<p>{COURSE_AUTHORS_LIST_IS_EMPTY}</p>
				) : (
					selectedAuthors.map((author) => {
						return (
							<div className={styles.AuthorsBlock} key={author.id}>
								<p className={styles.AuthorsBlockText}>{author.name}</p>
								<div>
									<Button
										label={BUTTON_DELETE_AUTHOR}
										buttonStyle={BUTTON_STYLE_PURPLE}
										handleClick={() => deleteAuthor(author)}
									/>
								</div>
							</div>
						);
					})
				)}
			</div>
		</div>
	);
};
