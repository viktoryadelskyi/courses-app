import { Link, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { getUser } from '../../store/selectors';
import { Logo } from './components/Logo';
import { HEADER_OUT_BTN, BUTTON_STYLE_PURPLE } from '../../constants';
import { Button } from '../../common';
import styles from './Header.module.css';
import { deleteUsers } from '../../store/user/thunk';
import { userSlice } from '../../store/user/userSlice';
const { userLogOut } = userSlice.actions;

export const Header = () => {
	const location = useLocation();
	const dispatch = useDispatch();

	const { name } = useSelector(getUser);

	const logOutHandler = () => {
		dispatch(deleteUsers());
		dispatch(userLogOut());
	};
	return (
		<>
			<div className={styles.Container}>
				<Logo />
				<div className={styles.UserBlock}>
					{location.pathname !== '/login' &&
						location.pathname !== '/registration' && (
							<>
								<p className={styles.Name}>{`${name}`}</p>
								<Link to='/login'>
									<Button
										label={HEADER_OUT_BTN}
										buttonStyle={BUTTON_STYLE_PURPLE}
										handleClick={logOutHandler}
									/>
								</Link>
							</>
						)}
				</div>
			</div>
		</>
	);
};
