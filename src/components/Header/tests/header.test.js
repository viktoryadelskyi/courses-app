import { Header } from '../Header';
import '@testing-library/jest-dom/extend-expect';
import { renderWithProviders } from '../../../helpers/renderWithRedux';
import { mockedUser } from '../../../store/user/userMock';

describe('test header', () => {
	test('logo on page', () => {
		const { queryByTestId } = renderWithProviders(<Header />);

		expect(queryByTestId('logo')).toBeInTheDocument();
	});

	test('name on page', () => {
		const { queryByText } = renderWithProviders(<Header />, {
			preloadedState: {
				user: mockedUser,
			},
		});

		expect(queryByText(/Test Name/)).toBeInTheDocument();
	});
});
