import React from 'react';
import logo from '../../../../assets/logo.png';
import styles from './Logo.module.css';

export const Logo = () => (
	<img src={logo} data-testid='logo' className={styles.img} alt='header-logo' />
);
