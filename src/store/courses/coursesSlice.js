import { createSlice } from '@reduxjs/toolkit';
import { fetchCourses, updateCourse } from './thunk';
import { DEFAULT_INITIAL_DURATION_VALUE } from '../../constants';
import { pipeDuration } from '../../helpers';

export const initialState = {
	courses: [],
	updatedCourse: [],
	foundedCourses: [],
	activeCourse: null,
	status: null,
	error: null,
	newCourseTitle: '',
	newCourseDescription: '',
	newCourseDurationTime: 0,
	searchInputValue: '',
	minutesNumber: DEFAULT_INITIAL_DURATION_VALUE,
};

export const coursesSlice = createSlice({
	name: 'courses',
	initialState,
	reducers: {
		setCourseTitle(state, action) {
			state.newCourseTitle = action.payload;
		},
		setCourseDescription(state, action) {
			state.newCourseDescription = action.payload;
		},
		setCourseDurationTime(state, action) {
			if (+action.payload > 0) {
				state.newCourseDurationTime = action.payload;
				state.minutesNumber = pipeDuration(action.payload);
			} else {
				state.newCourseDurationTime = '';
				state.minutesNumber = DEFAULT_INITIAL_DURATION_VALUE;
			}
		},
		addNewCourse(state, action) {
			state.courses.push(action.payload);
		},
		deleteCourseCard(state, action) {
			const newCourses = state.courses.filter(
				(item) => item.id !== action.payload
			);
			state.courses = newCourses;
		},
		setUpdatedCourse(state, action) {
			state.updatedCourse.push(action.payload);
		},
		setActiveCourse(state, action) {
			state.activeCourse = state.courses.find(
				(course) => course.id === action.payload
			);
		},
		resetCoursesInputs() {
			setCourseTitle('');
			setCourseDescription('');
			setCourseDurationTime('');
		},

		setSearchInputValue(state, action) {
			if (action.payload.length === 0) {
				state.foundedCourses = [];
			} else {
				state.searchInputValue = action.payload;
			}
		},
		setFoundedCourses(state) {
			state.foundedCourses = state.courses.filter((course) => {
				return (
					course.id.includes(state.searchInputValue) ||
					course.title.toLowerCase().includes(state.searchInputValue)
				);
			});
		},
	},
	extraReducers: {
		[fetchCourses.pending]: (state) => {
			state.status = 'loading';
			state.error = null;
		},
		[fetchCourses.fulfilled]: (state, action) => {
			state.status = 'resolved';
			state.courses = action.payload;
		},
		[fetchCourses.rejected]: (state, action) => {
			state.status = 'rejected';
			state.error = action.payload;
		},

		[updateCourse.pending]: (state) => {
			state.status = 'loading';
			state.error = null;
		},
		[updateCourse.fulfilled]: (state, action) => {
			state.status = 'resolved';
			const newCourses = state.courses.filter(
				(item) => item.id === action.payload
			);
			state.courses.push(newCourses);
		},
		[updateCourse.rejected]: (state, action) => {
			state.status = 'rejected';
			state.error = action.payload;
		},
	},
});

export const {
	setCourseTitle,
	setCourseDescription,
	setCourseDurationTime,
	addNewCourse,
	deleteCourseCard,
	setUpdatedCourse,
	setActiveCourse,
	setFoundedCourses,
	setSearchInputValue,
	resetCoursesInputs,
} = coursesSlice.actions;

export default coursesSlice.reducer;
