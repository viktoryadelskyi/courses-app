import { createAsyncThunk } from '@reduxjs/toolkit';
import {
	addCourses,
	deleteCourses,
	getCourses,
	updateCourses,
} from '../../servisces';

export const fetchCourses = createAsyncThunk(
	'courses/fetchCourses',
	async (_, { rejectWithValue }) => {
		try {
			const response = await getCourses();
			if (!response.data.successful) {
				throw new Error('Server Error');
			}
			const data = await response.data.result;
			return data;
		} catch (error) {
			return rejectWithValue(error.message);
		}
	}
);

export const deleteCourse = createAsyncThunk(
	'courses/deleteCourse',
	async (id, { rejectWithValue }) => {
		try {
			const response = await deleteCourses(id);
			if (!response.data.successful) {
				throw new Error('Server Error');
			}
		} catch (error) {
			return rejectWithValue(error.message);
		}
	}
);

export const updateCourse = createAsyncThunk(
	'courses/updateCourse',
	async (obj, { rejectWithValue }) => {
		try {
			const response = await updateCourses(obj.activeCourseId, obj.courseData);
			if (!response.data.successful) {
				throw new Error('Server Error');
			}
		} catch (error) {
			return rejectWithValue(error.message);
		}
	}
);

export const addCourse = createAsyncThunk(
	'courses/addCourse',
	async (course, { rejectWithValue }) => {
		try {
			const response = await addCourses(course);
			if (!response.data.successful) {
				throw new Error('Server Error');
			}
		} catch (error) {
			return rejectWithValue(error.message);
		}
	}
);
