import { combineReducers, configureStore } from '@reduxjs/toolkit';
import courseReducer from './courses/coursesSlice';
import userReducer from './user/userSlice';
import authorsReducer from './authors/authorsSlice';

export const rootReducer = combineReducers({
	courses: courseReducer,
	user: userReducer,
	authors: authorsReducer,
});

export const store = configureStore({
	reducer: rootReducer,
});

export const createReduxStore = (preloadedState) => {
	return configureStore({
		reducer: rootReducer,
		preloadedState,
	});
};
