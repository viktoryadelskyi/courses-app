import { createSlice } from '@reduxjs/toolkit';
import { fetchUsers } from './thunk';
const token = JSON.parse(localStorage.getItem('token'));

const initialState = {
	isAuth: !!token,
	name: token ? token.user.name : '',
	email: token ? token.user.name : '',
	password: '',
	token: token,
	role: '',
	status: null,
	error: null,
};

export const userSlice = createSlice({
	name: 'user',
	initialState,
	reducers: {
		userAuth(state, action) {
			state.isAuth = action.payload;
		},
		userName(state, action) {
			state.name = action.payload;
		},
		userEmail(state, action) {
			state.email = action.payload;
		},
		userToken(state, action) {
			state.token = action.payload;
		},
		userLogOut(state) {
			state.isAuth = false;
			state.email = '';
			state.name = '';
			state.token = '';
			state.role = '';
			localStorage.removeItem('token');
		},
		userPassword(state, action) {
			state.password = action.payload;
		},
	},
	extraReducers: {
		[fetchUsers.pending]: (state) => {
			state.status = 'loading';
			state.error = null;
		},
		[fetchUsers.fulfilled]: (state, action) => {
			state.role = action.payload.result.role;
			state.id = action.payload.result.id;
			state.email = action.payload.result.email;
			state.name = action.payload.result.name;
		},
		[fetchUsers.rejected]: (state, action) => {
			state.status = 'rejected';
			state.error = action.payload;
		},
	},
});

export const { userAuth, userName, userEmail, userToken, userLogOut } =
	userSlice.actions;
export default userSlice.reducer;
