import { createAsyncThunk } from '@reduxjs/toolkit';
import { getUser, deleteUser } from '../../servisces';

export const fetchUsers = createAsyncThunk(
	'user/fetchUsers',
	async (_, { rejectWithValue }) => {
		try {
			const response = await getUser();
			if (!response.data.successful) {
				throw new Error('Server Error');
			}
			const data = await response.data;
			return data;
		} catch (error) {
			return rejectWithValue(error.message);
		}
	}
);

export const deleteUsers = createAsyncThunk(
	'user/deleteUsers',
	async (_, { rejectWithValue }) => {
		try {
			const response = await deleteUser();
			if (!response.data.successful) {
				throw new Error('Server Error');
			}
		} catch (error) {
			return rejectWithValue(error.message);
		}
	}
);
