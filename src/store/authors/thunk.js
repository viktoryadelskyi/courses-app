import { createAsyncThunk } from '@reduxjs/toolkit';
import { addAuthors, getAuthors } from '../../servisces';

export const fetchAuthors = createAsyncThunk(
	'authors/fetchAuthors',
	async (_, { rejectWithValue }) => {
		try {
			const response = await getAuthors();
			if (!response.data.successful) {
				throw new Error('Server Error');
			}
			const data = await response.data.result;
			return data;
		} catch (error) {
			return rejectWithValue(error.message);
		}
	}
);

export const authorsAdd = createAsyncThunk(
	'authors/authorsAdd',
	async (name, { rejectWithValue }) => {
		try {
			const response = await addAuthors(name);
			if (!response.data.successful) {
				throw new Error('Server Error');
			}
			const data = await response.data.result;
			return data;
		} catch (error) {
			return rejectWithValue(error.message);
		}
	}
);
