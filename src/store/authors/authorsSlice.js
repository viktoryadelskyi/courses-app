import { createSlice } from '@reduxjs/toolkit';
import { fetchAuthors } from './thunk';

const initialState = {
	authors: [],
	selectedAuthors: [],
	newAuthorName: '',
	status: null,
	error: null,
};

export const authorsSlice = createSlice({
	name: 'authors',
	initialState,
	reducers: {
		setNewAuthorName(state, action) {
			state.newAuthorName = action.payload;
		},
		setSelectedAuthors(state, action) {
			if (action.payload) {
				state.selectedAuthors.push(action.payload);
				const newAuthors = state.authors.filter(
					(item) => item.id !== action.payload.id
				);
				state.authors = newAuthors;
			}
		},
		deleteAuthors(state, action) {
			state.authors.push(action.payload);
			const newSelectedAuthors = state.selectedAuthors.filter(
				(item) => item.id !== action.payload.id
			);
			state.selectedAuthors = newSelectedAuthors;
		},
		activeCourseSelectedAuthors(state, action) {
			if (action.payload) {
				state.selectedAuthors = state.authors.filter((item) =>
					action.payload.includes(item.id)
				);
				state.authors = state.authors.filter(
					(item) => !action.payload.includes(item.id)
				);
			}
		},
		resetAuthorsList(state) {
			const newAuthors = state.authors.concat(state.selectedAuthors);
			state.authors = newAuthors;
			state.selectedAuthors = [];
		},
	},
	extraReducers: {
		[fetchAuthors.pending]: (state) => {
			state.status = 'loading';
			state.error = null;
		},
		[fetchAuthors.fulfilled]: (state, action) => {
			state.status = 'resolved';
			state.authors = action.payload;
		},
		[fetchAuthors.rejected]: (state, action) => {
			state.status = 'rejected';
			state.error = action.payload;
		},
	},
});

export const {
	setNewAuthorName,
	setSelectedAuthors,
	deleteAuthors,
	activeCourseSelectedAuthors,
	resetAuthorsList,
} = authorsSlice.actions;
export default authorsSlice.reducer;
