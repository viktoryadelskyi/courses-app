import '@testing-library/jest-dom/extend-expect';
import { mockedCourses } from '../courses/coursesMock';
import coursesSlice, {
	addNewCourse,
	initialState,
} from '../courses/coursesSlice';
import { fetchCourses } from '../courses/thunk';

describe('test reducer', () => {
	test('reducer should return initial state', async () => {
		const actual = coursesSlice(initialState, {});

		expect(actual).toEqual(initialState);
	});

	test('reducer should handle SAVE_COURSE and returns new state.', () => {
		const action = addNewCourse(mockedCourses);
		const actual = coursesSlice(initialState, action);

		expect(actual).not.toEqual(initialState);
	});

	test('reducer should handle GET_COURSES and returns new state', async () => {
		const action = fetchCourses.fulfilled(mockedCourses);
		const actual = coursesSlice(initialState, action);

		expect(actual.status).toEqual('resolved');
		expect(actual.courses).toEqual(mockedCourses);
	});
});
